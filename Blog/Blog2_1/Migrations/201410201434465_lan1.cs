namespace Blog2_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            AlterColumn("dbo.Posts", "Title", c => c.String(maxLength: 500));
            AlterColumn("dbo.Tags", "Content", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropIndex("dbo.Accounts", new[] { "PostID" });
            DropForeignKey("dbo.Accounts", "PostID", "dbo.Posts");
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
            DropTable("dbo.Accounts");
        }
    }
}
