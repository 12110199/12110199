﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog1.Models
{
    public class Post
    {
        [Key]// truong nay thuc thi cho cai table neu khong co thi bao loi
        //tao ID
        public int Id { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }

    }
    public class BlogDbContext: DbContext
    {
        //tao ket noi csdl
        public DbSet<Post> Posts { set; get; }
        //lay toan bo table csdl co ten la Post de dua lentren controllers xu ly
    }
}