﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class News
    {
        public int NewsID { set; get; }
        public String Title { set; get; }
        [Required]
        [MinLength(50, ErrorMessage = "Body phai co it nhat 50 ky tu")]      
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DayUpdated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        
    }
}