﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Post
    {
        public int PostID { set; get; }
        public String Title { set; get; }
        [Required]
        [MinLength(50,ErrorMessage="Body phai co it nhat 50 ky tu")]      
        public String Body { set; get; }
        [Required]
        [StringLength(200,ErrorMessage="Trong Khoảng 50 đến 200 ký tự",MinimumLength=50)]
        public String Brief { set; get; }
        public String Author { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public int Views { set; get; }


        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public int CategoID { set; get; }
        public virtual Category Category { set; get; }
        public virtual ICollection<Rating> Rangtings { set; get; }

        
    }
}