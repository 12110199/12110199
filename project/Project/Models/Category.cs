﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Category
    {
        public int CategoryID { set; get; }
        public String NameCategory { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}