﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Rating
    {
        public int RatingID { set; get;}
        public int NuSelected { set; get; }
        public String Description { set; get; }
        public int PostId { set; get; }
        public virtual Post Post { set; get; }

    }
}