﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_6.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}