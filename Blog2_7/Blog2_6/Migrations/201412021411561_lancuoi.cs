namespace Blog2_6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lancuoi : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Posts", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
        }
    }
}
