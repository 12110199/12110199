﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class BlogDBContext:DbContext
    {
        public DbSet<Post> Posts { set; get; }
        //public DbSet<Comment> Comments { set; get; }


      protected override void OnModelCreating(DbModelBuilder modeBuilder)
        {
            base.OnModelCreating(modeBuilder);
            modeBuilder.Entity<Post>().HasMany(d => d.Tags).WithMany(p => p.Posts).Map(t => t.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Tag_Post"));
        }
        public DbSet<Tag> Tags { set; get; }

        public DbSet<Comment> Comments { get; set; }

    }
}